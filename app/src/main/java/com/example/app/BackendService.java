package com.example.app;
import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class BackendService {
    private static final String BASE_URL = "http://192.168.1.202:3002/user";
    private static final String ENDPOINT_STORE_ENTRY = "/storeEntry";

    private Activity activity;

    public BackendService(Activity activity) {
        this.activity = activity;
    }

    public void storeEntry(String scannedText,String cin,String id_local,String date,String heure) {
        // Create a JSON body with the scanned text
        JSONObject requestBodyJson = new JSONObject();
        try {
            requestBodyJson.put("clockToken",scannedText);
            requestBodyJson.put("cin",cin);
            requestBodyJson.put("id_local", id_local);
            requestBodyJson.put("date",date);
            requestBodyJson.put("heure",heure);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Create the HttpURLConnection object
        HttpURLConnection connection = null;
        try {
            URL url = new URL(BASE_URL + ENDPOINT_STORE_ENTRY);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(true);

            // Write the JSON data to the request body
            OutputStream outputStream = connection.getOutputStream();
            outputStream.write(requestBodyJson.toString().getBytes());
            outputStream.close();

            // Read the response from the server
            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                // Process the response
                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuilder responseBuilder = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    responseBuilder.append(line);
                }
                reader.close();

                // Display success message on the UI thread
                activity.runOnUiThread(() -> {
                    Toast.makeText(activity, "Entry stored successfully", Toast.LENGTH_SHORT).show();
                });
            } else {
                // Read the error response from the server
                BufferedReader errorReader = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
                StringBuilder errorResponseBuilder = new StringBuilder();
                String line;
                while ((line = errorReader.readLine()) != null) {
                    errorResponseBuilder.append(line);
                }
                errorReader.close();

                // Display error message on the UI thread
                final String errorResponse = errorResponseBuilder.toString();
                activity.runOnUiThread(() -> {
                    Toast.makeText(activity, "Failed to store entry: " + errorResponse, Toast.LENGTH_SHORT).show();
                });
            }
        } catch (IOException e) {
            e.printStackTrace();
            // Display error message on the UI thread
            activity.runOnUiThread(() -> {
                Toast.makeText(activity, "Failed to store entry", Toast.LENGTH_SHORT).show();
            });
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }
}
