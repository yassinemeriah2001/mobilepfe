package com.example.app;

import static android.app.Activity.RESULT_OK;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class QrCodeFragment extends Fragment {
    private Button scanButton;
    private TextView resultTextView;
    private BackendService backendService;
    private String cin;
    private String id_local;

    public QrCodeFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_qr_code, container, false);

        scanButton = view.findViewById(R.id.scan_button);
        resultTextView = view.findViewById(R.id.result_textview);
        backendService = new BackendService(getActivity());

        // Retrieve the cin value from arguments
        Bundle args = getArguments();
        if (args != null) {
            cin = args.getString("cin");
            id_local=args.getString("id_local");
        }

        Date Currentdate = new Date();
        SimpleDateFormat Timeformat = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        String Currenttime = Timeformat.format(Currentdate);

        // Determine if the scan button should be enabled or disabled
        if ((Currenttime.compareTo("08:00") >= 0 && Currenttime.compareTo("09:30") <= 0) ||
                (Currenttime.compareTo("12:00") >= 0 && Currenttime.compareTo("13:30") <= 0) ||
                (Currenttime.compareTo("14:00") >= 0 && Currenttime.compareTo("15:30") <= 0) ||
                (Currenttime.compareTo("17:00") >= 0 && Currenttime.compareTo("18:30") <= 0)) {
            scanButton.setEnabled(true);
        } else {
            scanButton.setEnabled(false);
        }


        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentIntegrator intentIntegrator = IntentIntegrator.forSupportFragment(QrCodeFragment.this);
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                intentIntegrator.setPrompt("Scan a QR code");
                intentIntegrator.setOrientationLocked(false);
                intentIntegrator.setBeepEnabled(false);
                intentIntegrator.initiateScan();
            }
        });

        return view;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IntentIntegrator.REQUEST_CODE && resultCode == RESULT_OK) {
            IntentResult intentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (intentResult != null) {
                String scanContent = intentResult.getContents();
                if (scanContent != null) {
                    resultTextView.setText(scanContent);
                    Toast.makeText(getActivity(), scanContent, Toast.LENGTH_SHORT).show();

                    // Get the current date and time
                    Date currentDate = new Date();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                    SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
                    String Date = dateFormat.format(currentDate);
                    String Time = timeFormat.format(currentDate);

                    // Determine the entry type based on the time
                    String Type;
                    String entryTime = Time.substring(0, 5); // Extract only the time part



                    // Call the backend service to store the entry
                    new StoreEntryTask(cin,id_local).execute(scanContent, Date, Time);
                } else {
                    Toast.makeText(getActivity(), "No QR code found", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private class StoreEntryTask extends AsyncTask<String, Void, Void> {
        private String cin;
        private String id_local;

        public StoreEntryTask(String cin,String id_local) {
            this.cin = cin;
            this.id_local=id_local;
        }
        @Override
        protected Void doInBackground(String... strings) {
            String scannedText = strings[0];
            String date = strings[1];
            String heure = strings[2];


            backendService.storeEntry(scannedText,cin,id_local, date, heure);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Toast.makeText(getActivity(), "Entry stored successfully", Toast.LENGTH_SHORT).show();
        }
    }
}
