package com.example.app;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import org.eazegraph.lib.charts.PieChart;
import org.eazegraph.lib.models.PieModel;

public class HomeFragment extends Fragment {

    private PieChart pieChart;
    private TextView totalDaysText;
    private Button unlockButton;

    private int totalDays = 0;
    private int absenceDays = 0;

    public HomeFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_home, container, false);

        // Initialize views
        pieChart = view.findViewById(R.id.chart);
        totalDaysText = view.findViewById(R.id.totalDaysTextView);
        unlockButton = view.findViewById(R.id.unlockButton);

        // Update total days and absence days (replace with your actual logic)
        totalDays = 50;
        absenceDays = 15;

        // Update the pie chart and total days text
        updateChart();
        updateTotalDays();

        // Check if the button should be enabled
        if (totalDays > 40 && (float) absenceDays / totalDays <= 0.3) {
            unlockButton.setEnabled(true);
        } else {
            unlockButton.setEnabled(false);
        }

        return view;
    }

    private void updateChart() {
        // Calculate percentage of absence days
        float absencePercentage = (float) absenceDays / totalDays;

        // Calculate percentage of work days
        float workPercentage = 1 - absencePercentage;

        // Set pie chart data
        pieChart.clearChart();
        pieChart.addPieSlice(new PieModel("Work", workPercentage,
                getResources().getColor(R.color.teal_700)));
        pieChart.addPieSlice(new PieModel("Absence", absencePercentage,
                getResources().getColor(R.color.purple_500)));

        // Set custom labels for the pie chart slices
        pieChart.startAnimation();
    }

    private void updateTotalDays() {
        totalDaysText.setText(String.valueOf(totalDays));
    }

    public void onUnlockButtonClick(View view) {
        // Handle unlock button click
        // Add your code here
        Toast.makeText(getActivity(), "Clicked", Toast.LENGTH_SHORT).show();
    }
}
