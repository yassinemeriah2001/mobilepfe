package com.example.app;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;


public class ConcoursFragment extends Fragment {



    public ConcoursFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

         View view=inflater.inflate(R.layout.activity_concours, container, false);


        TableLayout eventsTable = view.findViewById(R.id.events_table);



        String[] eventNames = {"Concour 1", "Concour 2", "Concour 3", "Concour 4"};
        int eventCount = eventNames.length;


        for (int i = 0; i < eventCount; i++) {

            TableRow eventRow = new TableRow(getActivity());


            TextView eventNameTextView = new TextView(getActivity());
            eventNameTextView.setLayoutParams(new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, 1));
            eventNameTextView.setText(eventNames[i]);
            eventNameTextView.setTextSize(16);
            eventRow.addView(eventNameTextView);


            Button participateButton = new Button(getActivity());
            participateButton.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
            participateButton.setText("Participate");
            participateButton.setGravity(Gravity.CENTER);
            participateButton.setTextColor(Color.parseColor("#fdc613"));
            participateButton.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#081d5c")));
            eventRow.addView(participateButton);

            final int eventIndex = i;
            participateButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (eventIndex == 0) {
                        // Add your specific action for Event 1 here
                        Toast.makeText(getActivity(), "Participate button clicked for Event 1", Toast.LENGTH_SHORT).show();
                    }
                }
            });


            eventsTable.addView(eventRow);

    }
        return view;
}}