package com.example.app;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SettingsFragment extends Fragment {




        public SettingsFragment() {

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            // Inflate the layout for this fragment

            View view = inflater.inflate(R.layout.activity_settings, container, false);

            EditText emailEditText = view.findViewById(R.id.email_edit_text);
            EditText passwordEditText = view.findViewById(R.id.password_edit_text);
            EditText confirmPasswordEditText = view.findViewById(R.id.confirm_password_edit_text);
            Button updateButton = view.findViewById(R.id.update_button);

            updateButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Retrieve entered data
                    String email = emailEditText.getText().toString();
                    String password = passwordEditText.getText().toString();
                    String confirmPassword = confirmPasswordEditText.getText().toString();

                    // Check if password matches confirm password
                    if (password.isEmpty() || email.isEmpty() || confirmPassword.isEmpty()) {
                        Toast.makeText(getActivity(), "All text fields must written", Toast.LENGTH_SHORT).show();
                    } else if (password.equals(confirmPassword)) {
                        // Passwords match, perform update action
                        // ...
                        Toast.makeText(getActivity(), "Updated", Toast.LENGTH_SHORT).show();
                    } else {
                        // Passwords do not match, display error message
                        confirmPasswordEditText.setError("Error Occured");
                        confirmPasswordEditText.requestFocus();
                    }
                }
            });
        return view;}

        }


