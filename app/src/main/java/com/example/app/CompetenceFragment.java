package com.example.app;

import android.Manifest;
import android.content.ClipData;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.pdf.PdfRenderer;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import java.io.IOException;

public class CompetenceFragment extends Fragment {

    private static final int REQUEST_CAMERA_PERMISSION = 200;
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int REQUEST_DOCUMENT = 2;

    private ImageView photoImageView;

    public CompetenceFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_competence, container, false);
        photoImageView = view.findViewById(R.id.photoImageView);
        Button selectPhotoButton = view.findViewById(R.id.selectPhotoButton);
        Button takePhotoButton = view.findViewById(R.id.takePhotoButton);
        Button selectDocumentButton = view.findViewById(R.id.selectDocumentButton);

        selectPhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Check if the required permission is granted
                if (ContextCompat.checkSelfPermission(requireActivity(),
                        Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    // Start the gallery to select a photo
                    startGallery();
                } else {
                    // Request the permission
                    ActivityCompat.requestPermissions(requireActivity(),
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            REQUEST_CAMERA_PERMISSION);
                }
            }
        });

        takePhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Check if the required permission is granted
                if (ContextCompat.checkSelfPermission(requireActivity(),
                        Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    // Start the camera to take a photo
                    startCamera();
                } else {
                    // Request the permission
                    ActivityCompat.requestPermissions(requireActivity(),
                            new String[]{Manifest.permission.CAMERA},
                            REQUEST_CAMERA_PERMISSION);
                }
            }
        });
        selectDocumentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Check if the required permission is granted
                if (ContextCompat.checkSelfPermission(requireActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    // Start the document picker
                    startDocumentPicker();
                } else {
                    // Request the permission
                    ActivityCompat.requestPermissions(requireActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_DOCUMENT);
                }
            }
        });

        return view;
    }

    private void startGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
    }

    private void startCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
    }
    private void startDocumentPicker() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,text/plain");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, REQUEST_DOCUMENT);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == getActivity().RESULT_OK) {
            if (data != null) {
                ClipData clipData = data.getClipData();
                if (clipData != null) {
                    // Multiple images selected
                    for (int i = 0; i < clipData.getItemCount(); i++) {
                        Uri photoUri = clipData.getItemAt(i).getUri();
                        // Handle each selected image
                        handleSelectedImage(photoUri);
                    }
                } else if (data.getData() != null) {
                    // Single image selected
                    Uri photoUri = data.getData();
                    handleSelectedImage(photoUri);
                } else if (data.getExtras() != null) {
                    // Taken image from camera
                    Bitmap imageBitmap = (Bitmap) data.getExtras().get("data");
                    photoImageView.setImageBitmap(imageBitmap);
                }
            }
        } else if (requestCode == REQUEST_DOCUMENT && resultCode == getActivity().RESULT_OK) {
            if (data != null) {
                Uri documentUri = data.getData();
                handleSelectedDocument(documentUri);
            }
        }
    }


    private void handleSelectedImage(Uri photoUri) {
        try {
            Bitmap imageBitmap = MediaStore.Images.Media.getBitmap(requireActivity().getContentResolver(), photoUri);
            // Handle the selected image as needed (e.g., display it, save it, etc.)
            photoImageView.setImageBitmap(imageBitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void handleSelectedDocument(Uri documentUri) {
        try {
            ParcelFileDescriptor fileDescriptor = requireActivity().getContentResolver().openFileDescriptor(documentUri, "r");
            if (fileDescriptor != null) {
                PdfRenderer renderer = new PdfRenderer(fileDescriptor);

                int pageCount = renderer.getPageCount();
                Bitmap[] bitmaps = new Bitmap[pageCount];

                for (int i = 0; i < pageCount; i++) {
                    PdfRenderer.Page page = renderer.openPage(i);
                    Bitmap bitmap = Bitmap.createBitmap(page.getWidth(), page.getHeight(), Bitmap.Config.ARGB_8888);
                    page.render(bitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
                    bitmaps[i] = bitmap;
                    page.close();
                }

                ImageView imageView = requireView().findViewById(R.id.photoImageView);
                imageView.setImageBitmap(bitmaps[0]);

                // Implement scrolling through pages
                imageView.setOnTouchListener(new View.OnTouchListener() {
                    float startX;
                    int currentPage = 0;

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction()) {
                            case MotionEvent.ACTION_DOWN:
                                startX = event.getX();
                                return true;

                            case MotionEvent.ACTION_UP:
                                float endX = event.getX();
                                if (startX - endX > 0 && currentPage < pageCount - 1) {
                                    // Swipe left to move to the next page
                                    currentPage++;
                                    imageView.setImageBitmap(bitmaps[currentPage]);
                                } else if (startX - endX < 0 && currentPage > 0) {
                                    // Swipe right to move to the previous page
                                    currentPage--;
                                    imageView.setImageBitmap(bitmaps[currentPage]);
                                }
                                return true;
                        }
                        return false;
                    }
                });

                renderer.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }






    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted, start the gallery
                startGallery();
            }
        } else if (requestCode == REQUEST_DOCUMENT) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted, start the document picker
                startDocumentPicker();
            }
        }
    }
    }

