package com.example.app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;


public class EmployeeNav extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emloyee_nav);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);

        getSupportActionBar().setTitle("Menu");
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#081d5c")));

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                // Handle navigation view item clicks here.
                int id = menuItem.getItemId();

                // Check which menu item was clicked and do something.
                if (id == R.id.nav_home) {
                    getSupportActionBar().setTitle("Home");
                    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#081d5c")));
                    Fragment fragment = new HomeFragment();
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.main_content, fragment);
                    ft.commit();
                     }
                else if (id == R.id.nav_QrCode) {
                    getSupportActionBar().setTitle("Scan Qr");
                    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#081d5c")));
                    Fragment fragment = new QrCodeFragment();
                    String cin = getIntent().getStringExtra("cin");
                    String id_local=getIntent().getStringExtra("id_local");
                    Bundle bundle = new Bundle();
                    bundle.putString("cin", cin); // Pass the cin variable
                    bundle.putString("id_local",id_local);
                    fragment.setArguments(bundle);
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.main_content, fragment);
                    ft.commit();
                } else if (id == R.id.nav_settings) {
                    getSupportActionBar().setTitle("Settings");
                    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#081d5c")));
                    Fragment fragment = new SettingsFragment();
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.main_content, fragment);
                    ft.commit();
                }else if (id == R.id.nav_concours) {
                    getSupportActionBar().setTitle("Concours");
                    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#081d5c")));
                    Fragment fragment = new ConcoursFragment();
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.main_content, fragment);
                    ft.commit();
                }
                else if (id == R.id.nav_competence) {
                    getSupportActionBar().setTitle("Competence");
                    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#081d5c")));
                    Fragment fragment = new CompetenceFragment();
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.main_content, fragment);
                    ft.commit();
                }

                DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
